﻿/// TODO
/// RELEASE CHECKLIST
/// check time units
/// check logger
/// 

using BytexDigital.BattlEye.Rcon;
using BytexDigital.BattlEye.Rcon.Commands;
using BytexDigital.BattlEye.Rcon.Domain;



using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using DZRCon.Properties;
using System.Configuration;
//using System.Windows.Media;

using AndreyIM_Log;
using System.Diagnostics;
using System.IO;

namespace DZRCon
{
    public partial class Form1 : Form
    {
        class Globals
        {
            public static bool LogConnectOnce { get; set; }
            public static bool mainLoop { get; set; }
            public static bool RunBat { get; set; }
            public static bool Runner { get; set; }
            public static bool RunningBat { get; set; }
            public static bool locked { get; set; }
            public static int maxPing { get; set; }
            public static bool connected { get; set; }
            public static bool connecting { get; set; }
            public static bool waitingConnection { get; set; }
            public static bool waitingPlayers { get; set; }
            public static bool startedWork { get; set; }
            public static Color theColor { get; set; }
            public static RconClient networkClient { get; set; }
            public static List<Player> onlinePlayers { get; set; }
            public static string ConnectionText1 { get; set; }
            public static string ConnectionText2 { get; set; }
            public static string ConnectionText3 { get; set; }
            public static int Countdown { get; set; }
            public static CancellationTokenSource CancelToken { get; set; }
            public static System.Windows.Forms.Label CountdownLabel { get; set; }

        }

        public Form1()
        {
            InitializeComponent();
            Logger.RichTextBoxLogger = RconConsole;

            Globals.connected = false;
            Globals.RunningBat = false;
            Globals.Runner = false;
            Globals.locked = false;
            Globals.maxPing = 800;
            Globals.mainLoop = true;
            Globals.connecting = false;
            Globals.waitingPlayers = false;
            Globals.waitingConnection = false;
            Globals.CancelToken = new CancellationTokenSource();
            Globals.RunBat = false;
            Globals.LogConnectOnce = true;
            //
            //System.Windows.Forms.MessageBox.Show("this.rconIP.Text:" + this.rconIP.Text.ToString());
        }

        private async void button1_Click(object sender, EventArgs e)
        {

            Globals.networkClient = new RconClient(this.rconIP.Text, int.Parse(this.rconPort.Text), this.rconPass.Text);
            //Globals.networkClient.Connected += NetworkClient_Connected;
            Globals.networkClient.Disconnected += NetworkClient_Disconnected;
            Globals.networkClient.MessageReceived += NetworkClient_MessageReceived;
            Globals.networkClient.PlayerConnected += NetworkClient_PlayerConnected;
            Globals.networkClient.PlayerDisconnected += NetworkClient_PlayerDisconnected;
            Globals.networkClient.PlayerRemoved += NetworkClient_PlayerRemoved;
            StartProgram.BackColor = Color.Orange;
            StartProgram.Text = "Connecting";
            StartProgram.Enabled = false;

            rconIP.Text = Settings.Default["rconIP"].ToString();
            rconPort.Text = Settings.Default["rconPort"].ToString();
            rconPass.Text = Settings.Default["rconPass"].ToString();

            Message1time.Text = Settings.Default["Message1Time"].ToString();
            Message2time.Text = Settings.Default["Message2Time"].ToString();
            Message3time.Text = Settings.Default["Message3Time"].ToString();
            Message1Text.Text = Settings.Default["Message1Text"].ToString();
            Message2Text.Text = Settings.Default["Message2Text"].ToString();
            Message3Text.Text = Settings.Default["Message3Text"].ToString();

            LockTime.Text = Settings.Default["LockTime"].ToString();
            KickTime.Text = Settings.Default["KickTime"].ToString();
            RestartTime.Text = Settings.Default["RestartTime"].ToString();
            RunBatTime.Text = Settings.Default["RunBatTime"].ToString();
            BatName.Text = Settings.Default["BatName"].ToString();

            //countdown loop
            launchTimer();

            //
            //Logger.WriteLog("WTF? " + Globals.waitingPlayers);
            TryConnecting2();
            //COLORIZE
            int TimeMultiplier = 1000 * 60;
            //int TimeMultiplier = 1000;


            while (true) //Globals.mainLoop
            {

                await Task.Delay(3000); //preventive
                int delayTime;
                //if (repeatRestarts.Checked) { repeatRestarts.BackColor = Globals.theColor; } else { repeatRestarts.BackColor = Color.DarkGray; }
                if (Globals.connected)
                {
                    try
                    {
                        // MAIN EXECUTION           
                        PlayersCountPanel.Visible = true;
                        StartProgram.BackColor = Color.Lime;
                        StartProgram.Text = "ONLINE";
                        Globals.networkClient.Send(new UnlockCommand());

                        Color newColor = Color.Cyan;
                        Globals.Countdown = 0;
                        #region newColors
                        if (EnableM1.Checked) { M1panel.BackColor = newColor; } else { M1panel.BackColor = Color.DarkGray; }
                        if (EnableM2.Checked) { M2panel.BackColor = newColor; } else { M1panel.BackColor = Color.DarkGray; }
                        if (EnableM3.Checked) { M3panel.BackColor = newColor; } else { M1panel.BackColor = Color.DarkGray; }
                        if (EnableLock.Checked) { LockPanel.BackColor = newColor; } else { LockPanel.BackColor = Color.DarkGray; }
                        if (EnableKick.Checked) { KickPanel.BackColor = newColor; } else { KickPanel.BackColor = Color.DarkGray; }
                        if (EnableRestart.Checked) { RestartPanel.BackColor = newColor; } else { RestartPanel.BackColor = Color.DarkGray; }
                        #endregion

                        string sendMessage;

                        //Tasks
                        // 1
                        //runDown("Cue", 1);
                       

                        if (EnableM1.Checked && Globals.connected)
                        {
                            M1panel.BackColor = Color.Orange;
                            delayTime = int.Parse(Message1time.Text) * TimeMultiplier;
                            //Logger.WriteLog("WAITING TIME: " + Message1time.Text);
                            Globals.Countdown = delayTime;
                            getServerPlayers();
                            await Task.Delay(delayTime, Globals.CancelToken.Token).ContinueWith(tsk => { });


                            if (Globals.connected)
                            {
                                //runDown("Play", 1);
                                M1panel.BackColor = Color.Lime;
                                if (EnableM2.Checked) { M2panel.BackColor = Color.Orange;  };
                                sendMessage = String.Format(Message1Text.Text, delayTime);
                                Globals.networkClient.Send(new SendMessageCommand(sendMessage));
                                getServerPlayers();
                            }
                        }

                        // 2
                        if (EnableM2.Checked && Globals.connected)
                        {
                            M2panel.BackColor = Color.Orange;
                            delayTime = int.Parse(Message2time.Text) * TimeMultiplier;
                            //Logger.WriteLog("WAITING TIME: " + Message2time.Text);
                            Globals.Countdown = delayTime;
                            getServerPlayers();
                            await Task.Delay(delayTime, Globals.CancelToken.Token).ContinueWith(tsk => { });

                            if (Globals.connected)
                            {
                                //runDown("Play", 2);
                                M2panel.BackColor = Color.Lime;
                                if (EnableM3.Checked) { M3panel.BackColor = Color.Orange; };
                                sendMessage = String.Format(Message2Text.Text, delayTime);
                                Globals.networkClient.Send(new SendMessageCommand(sendMessage));
                                getServerPlayers();
                            }
                        }

                        // 3
                        if (EnableM3.Checked && Globals.connected)
                        {
                            M3panel.BackColor = Color.Orange;
                            delayTime = int.Parse(Message3time.Text) * TimeMultiplier;
                            //Logger.WriteLog("WAITING TIME: " + Message3time.Text);
                            Globals.Countdown = delayTime;
                            getServerPlayers();
                            await Task.Delay(delayTime, Globals.CancelToken.Token).ContinueWith(tsk => { });

                            if (Globals.connected)
                            {
                                //runDown("Play", 3);
                                M3panel.BackColor = Color.Lime;
                                if (EnableLock.Checked) { LockPanel.BackColor = Color.Orange; };
                                sendMessage = String.Format(Message3Text.Text, delayTime);
                                Globals.networkClient.Send(new SendMessageCommand(sendMessage));
                                getServerPlayers();
                            }
                        }


                        // Lock
                        if (EnableLock.Checked && Globals.connected)
                        {
                            //runDown("Cue", 4);
                            LockPanel.BackColor = Color.Orange;
                            delayTime = int.Parse(LockTime.Text) * TimeMultiplier;
                            //Logger.WriteLog("WAITING TIME: " + LockTime.Text);
                            Globals.Countdown = delayTime;
                            getServerPlayers();
                            await Task.Delay(delayTime, Globals.CancelToken.Token).ContinueWith(tsk => { });

                            if (Globals.connected)
                            {
                                //runDown("Play", 4);
                                LockPanel.BackColor = Color.Lime;
                                if (EnableKick.Checked) { KickPanel.BackColor = Color.Orange; };
                                Globals.networkClient.Send(new LockServerCommand());
                                getServerPlayers();
                            }
                        }

                        // Kick
                        if (EnableKick.Checked && Globals.connected)
                        {
                            //runDown("Cue", 5);
                            KickPanel.BackColor = Color.Orange;
                            delayTime = int.Parse(KickTime.Text) * TimeMultiplier;
                            //Logger.WriteLog("WAITING TIME: " + KickTime.Text);
                            Globals.Countdown = delayTime;
                            getServerPlayers();
                            await Task.Delay(delayTime, Globals.CancelToken.Token).ContinueWith(tsk => { });
                            if (Globals.connected)
                            {
                                //runDown("Play", 5);
                                KickPanel.BackColor = Color.Lime;
                                if (EnableRestart.Checked) { RestartPanel.BackColor = Color.Orange; };
                                kickAllPlayers();
                                getServerPlayers();
                            }
                        }


                        // Restart
                        if (EnableRestart.Checked && Globals.connected)
                        {
                            //runDown("Cue", 6);
                            RestartPanel.BackColor = Color.Orange;
                            delayTime = int.Parse(RestartTime.Text) * TimeMultiplier;
                            //Logger.WriteLog("WAITING TIME: " + RestartTime.Text);
                            Globals.Countdown = delayTime;
                            getServerPlayers();
                            await Task.Delay(delayTime, Globals.CancelToken.Token).ContinueWith(tsk => { }); ;
                            if (Globals.connected)
                            {
                                //runDown("Play", 6);
                                RestartPanel.BackColor = Color.Lime;
                                if (EnableRun.Checked) { runPanel.BackColor = Color.Orange; };
                                Globals.networkClient.Send(new ShutdownCommand());

                            }
                        }

                        //await Task.Delay(2000, Globals.CancelToken.Token).ContinueWith(tsk => { }); ;
                        // MAIN EXECUTION
                    }
                    catch (SocketException)
                    {
                        Logger.WriteLog("No internet! App will exit!");
                    }
                    catch (NullReferenceException)
                    {
                        Logger.WriteLog("Disconnected NullReferenceException");
                    }

                }
                else
                {
                    PlayersCountPanel.Visible = false;
                    StartProgram.BackColor = Color.Orange;
                    StartProgram.Text = "Connecting";
                    //Globals.theColor = Color.Cyan;

                    Color newColor = Color.Cyan;
                    #region newColors
                    M1panel.BackColor = newColor;
                    M2panel.BackColor = newColor;
                    M3panel.BackColor = newColor;
                    LockPanel.BackColor = newColor;
                    KickPanel.BackColor = newColor;
                    RestartPanel.BackColor = newColor;
                    runPanel.BackColor = newColor;
                    #endregion

                    //await Task.Delay(3000);
                    
                    if (!Globals.connected && Globals.LogConnectOnce)
                    {
                        RconConsole.Clear();
                        Logger.WriteLog(Globals.ConnectionText1);
                        Logger.WriteLog(Globals.ConnectionText2);
                        Logger.WriteLog(Globals.ConnectionText3);
                        Globals.LogConnectOnce = false;
                        //await Task.Delay(5000);
                    }

                    //Logger.WriteLog("Reconnecting in 5 seconds");
                    //await Task.Delay(5000);
                    if (!Globals.connected)
                    {
                        TryConnecting2();

                        if (EnableRun.Checked)
                        {
                            //runDown("Cue", 7);
                            //Globals.RunBat = true;

                            delayTime = int.Parse(RunBatTime.Text) * TimeMultiplier;

                            //Logger.WriteLog("WAITING TIME: " + RunBatTime.Text);

                            runPanel.BackColor = Color.Orange;
                            if (runBatFile(delayTime))
                            {
                                runPanel.BackColor = Color.Lime;
                                //if (EnableM1.Checked) { M2panel.BackColor = Color.Orange; };
                            }
                        }

                    }


                }

               // Logger.WriteLog("Globals.RunBat: " + Globals.RunBat);
               // Logger.WriteLog("Globals.connected: " + Globals.connected);
                //Logger.WriteLog("Globals.connecting: " + Globals.connecting);
            }






            // Task ignoredAwaitableResult  = rconSay("#uptime", networkClient, 3);
            // Task ignoredAwaitableResult2  = rconSay("Рестарт через 5 секунд", networkClient, 5000);

        }

         private async void launchTimer()
        {
            bool threading = true;
            TimeSpan time;
            //new Thread(() =>
            //{
            // Thread.CurrentThread.IsBackground = true;
            while (threading)
            {
                
                time = TimeSpan.FromMilliseconds(Globals.Countdown);
                string formTime = "-" + time.ToString(@"hh\:mm\:ss");
                //Logger.WriteLog("CountingDown: " + formTime + "ms: "+ Globals.Countdown);
                Globals.CountdownLabel.Text = formTime;
                Globals.Countdown = Globals.Countdown - 1000;
                if (Globals.Countdown <= 1)
                {
                    Globals.Countdown = 0;
                    countdownText.ForeColor = Color.DarkGray;
                    countdownText.Font = new Font(countdownText.Font, FontStyle.Regular);

                } else
                {
                    //Color myRgbColor = new Color();
                    //Color myRgbColor = new Color();
                    //myRgbColor = Color.FromRgb(0, 255, 0);
                    countdownText.ForeColor = Color.FromArgb(1,255, 120, 0);
                    countdownText.Font = new Font(countdownText.Font, FontStyle.Bold);
                }
                await Task.Delay(1000);
                /*
                if (runPanel.BackColor != Color.Lime && Globals.connected)
                {
                    if (Globals.RunBat) { runPanel.BackColor = Color.Lime; } else { runPanel.BackColor = Globals.theColor; }
                }
                if (Globals.connected)
                {
                    Globals.theColor = Color.Orange;
                    if (EnableM1.Checked) { if (M1panel.BackColor != Color.Lime) { M1panel.BackColor = Globals.theColor; } } else { M1panel.BackColor = Color.DarkGray; }
                    if (EnableM2.Checked) { if (M2panel.BackColor != Color.Lime) { M2panel.BackColor = Globals.theColor; } } else { M2panel.BackColor = Color.DarkGray; }
                    if (EnableM3.Checked) { if (M3panel.BackColor != Color.Lime) { M3panel.BackColor = Globals.theColor; } } else { M3panel.BackColor = Color.DarkGray; }
                    if (EnableLock.Checked) { if (LockPanel.BackColor != Color.Lime) { LockPanel.BackColor = Globals.theColor; } } else { LockPanel.BackColor = Color.DarkGray; }
                    if (EnableKick.Checked) { if (KickPanel.BackColor != Color.Lime) { KickPanel.BackColor = Globals.theColor; } } else { KickPanel.BackColor = Color.DarkGray; }
                    if (EnableRestart.Checked) { if (RestartPanel.BackColor != Color.Lime) { RestartPanel.BackColor = Globals.theColor; } } else { RestartPanel.BackColor = Color.DarkGray; }

                }
                else
                {
                    Globals.theColor = Color.Cyan;
                    if (EnableM1.Checked) { M1panel.BackColor = Globals.theColor; } else { M1panel.BackColor = Color.DarkGray; }
                    if (EnableM2.Checked) { M2panel.BackColor = Globals.theColor; } else { M2panel.BackColor = Color.DarkGray; }
                    if (EnableM3.Checked) { M3panel.BackColor = Globals.theColor; } else { M3panel.BackColor = Color.DarkGray; }
                    if (EnableLock.Checked) { LockPanel.BackColor = Globals.theColor; } else { LockPanel.BackColor = Color.DarkGray; }
                    if (EnableKick.Checked) { KickPanel.BackColor = Globals.theColor; } else { KickPanel.BackColor = Color.DarkGray; }
                    if (EnableRestart.Checked) { RestartPanel.BackColor = Globals.theColor; } else { RestartPanel.BackColor = Color.DarkGray; }
                }
                */
            }
            //}).Start();
        }

        private bool runBatFile(int v, bool manual = false)
        {
            bool result = false;
            if (File.Exists(BatName.Text))
            {
                //throw new NotImplementedException();
                // Logger.WriteLog("Runbat start");
                //  Logger.WriteLog("Globals.RunBat: " + Globals.RunBat);
                //  Logger.WriteLog("Globals.connected: " + Globals.connected);
                /*
                if (killExe.Checked)
                {
                    foreach (var process in Process.GetProcessesByName("DayZDiag_x64"))
                    {
                        process.Kill();
                    }
                }
               */
                //bool Globals.Runner;
                if (manual)
                {
                    Globals.Runner = manual;
                }
                else
                {
                    Globals.Runner = Globals.RunBat;
                }
                new Thread(() =>
                {
                    //ManualRun.Enabled = false;

                    Thread.CurrentThread.IsBackground = true;

                    if (Globals.Runner && !Globals.connected)
                    {
                        //runDown("Play", 7);
                        Globals.RunBat = false;
                        Globals.Runner = false;
                        Globals.Countdown = v;
                        Thread.Sleep(v);
                        Process proc = null;
                        try
                        {
                            string batDir = Directory.GetCurrentDirectory();
                            proc = new Process();
                            proc.StartInfo.WorkingDirectory = batDir;
                            proc.StartInfo.FileName = BatName.Text;
                            proc.StartInfo.CreateNoWindow = false;
                            proc.Start();
                            proc.WaitForExit();

                            Logger.WriteLog(BatName.Text + " executed");
                            result = true;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.StackTrace.ToString());
                        }
                    }
                }).Start();
            }
            else
            {
                System.Windows.Forms.MessageBox.Show("File does not exist. Check path.");
            }
            return result;

        }

        private void kickAllPlayers()
        {
            foreach (var player in Globals.onlinePlayers)
            {
                Globals.networkClient.Send(new KickCommand(player.Id));
            }
        }

        private void getServerPlayers()
        {
            // Logger.WriteLog("Waiting player list. waitingPlayers: "+ Globals.waitingPlayers);
            if (!Globals.waitingPlayers)
            {
                new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;

                    Globals.waitingPlayers = true;
                    bool requestSuccess = Globals.networkClient.Fetch(
                        command: new GetPlayersRequest(),
                        timeout: 5000,
                        result: out List<Player> onlinePlayers);
                    //Logger.WriteLog("Waiting for request success. waitingPlayers: " + Globals.waitingPlayers);
                    if (requestSuccess)
                    {
                        Globals.onlinePlayers = onlinePlayers;
                        // Logger.WriteLog($"Players online: {onlinePlayers.Count}");
                        //Logger.WriteLog($"Players 0: {onlinePlayers[0].Name}");
                        Globals.waitingPlayers = false;

                        //
                        //                       ping,
                        //guid,
                        //name,
                        //isVerified,
                        //isInLobby
                        PlayerCount.Text = onlinePlayers.Count.ToString();
                    }

                }).Start();
            }

        }

        private void TryConnecting2() //////////////// 2222222222222222222
        {

            //Colorize the playlist
            //Globals.Countdown = 0;
            // condition? consequent : alternative
            //SchedulePanel.Enabled = false;
            //SettingsPanel.Enabled = false;
            string Conneciton = this.rconIP.Text + ":" + int.Parse(this.rconPort.Text);

            //Logger.WriteLog("Trying to connect: " + Conneciton);
            if (!Globals.connected && !Globals.connecting)
            {
                new Thread(() =>
                {
                    Thread.CurrentThread.IsBackground = true;
                    Globals.connecting = true;
                    while (true)
                    {
                        Globals.ConnectionText1 = "Trying to connect: " + Conneciton;

                        if (Globals.connected)
                        {
                            //Logger.WriteLog("Connected! Aborting Thread!");
                            //Globals.connecting = false;
                            Thread.CurrentThread.Abort();

                        }
                        try
                        {

                            Globals.networkClient.Connect();

                            // Globals.waitingConnection = false;
                            Globals.connected = Globals.networkClient.WaitUntilConnected(5000);
                            Globals.connecting = !Globals.connected;
                            if (!Globals.connected)
                            {
                                Globals.ConnectionText2 = "Connection failed.";
                                Globals.ConnectionText3 = "The server my be still loading, connection occurs after[CE][Hive] :: Init sequence finished. Otherwise, check IP, port, and password.)";
                                //Globals.RunBat = true;
                            }
                            else
                            {
                                Globals.CancelToken.Dispose();
                                Globals.CancelToken = new CancellationTokenSource();
                                //Globals.theColor = Color.Orange;
                                Logger.WriteLog("CONNECTED: " + Conneciton);
                                Globals.ConnectionText1 = "";
                                Globals.ConnectionText2 = "";
                                Globals.ConnectionText3 = "";
                                Globals.RunBat = false;
                            }
                        }
                        catch (SocketException)
                        {
                            Logger.WriteLog("No internet! Retry when you're back online.");
                        }


                        // wait  async 
                        //Logger.WriteLog("connected:" + Globals.connected);
                        // Logger.WriteLog("waitingConnection:" + Globals.waitingConnection);





                        //Thread.Sleep(3000);

                    }
                }).Start();
            }



        }

        private async Task rconShutdown(string text, RconClient networkClient, int delay_s = 0)
        {
            await Task.Delay(delay_s * 1000);
            networkClient.Send(new SendMessageCommand(text));
        }

        private async Task rconSay(string text, RconClient networkClient, int delay = 0)
        {
            await Task.Delay(delay);
            networkClient.Send(new SendMessageCommand(text));
        }

        private static void NetworkClient_PlayerRemoved(object sender, BytexDigital.BattlEye.Rcon.Events.PlayerRemovedArgs e)
        {
            Logger.WriteLog($"Player {e.Name} ({e.Id}) with guid {e.Guid} was removed ({(e.IsBan ? "Ban" : "Kick")}) with reason: {e.Reason}");
        }

        private static void NetworkClient_PlayerDisconnected(object sender, BytexDigital.BattlEye.Rcon.Events.PlayerDisconnectedArgs e)
        {
            Logger.WriteLog($"Player {e.Name} ({e.Id}) disconnted");
        }

        private static void NetworkClient_PlayerConnected(object sender, BytexDigital.BattlEye.Rcon.Events.PlayerConnectedArgs e)
        {
            Logger.WriteLog($"Player {e.Name} ({e.Id}) joined with guid {e.Guid}");
        }

        private static void NetworkClient_MessageReceived(object sender, string e)
        {
            Logger.WriteLog("Server message: " + e);
        }

        private static void NetworkClient_Disconnected(object sender, EventArgs e)
        {
            Globals.RunBat = true;
            Globals.connected = false;
            Globals.connecting = false;
            Globals.waitingConnection = true;
            Globals.waitingPlayers = true;
            //Globals.theColor = Color.Cyan;
            Globals.CancelToken.Cancel();
            //Globals.CancelToken.CancelAfter()
            Logger.WriteLog("Disconnected");
            Globals.LogConnectOnce = true;
            Globals.Countdown = 0;
        }

        private static void NetworkClient_Connected(object sender, EventArgs e)
        {
            Globals.connected = true;
            //Globals.mainLoop = true;
            Globals.connecting = false;
            Globals.waitingConnection = false;
            Globals.waitingPlayers = false;
            Globals.RunBat = false;
            //runDown("Load", 7);
            Logger.WriteLog("Connected");
            Globals.LogConnectOnce = true;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            saveSettings();
            Application.Exit();
        }

        private Settings saveSettings()
        {
            try
            {

                Settings.Default.Save();
            }
            catch (System.FormatException)
            {
                System.Windows.Forms.MessageBox.Show("Fields cannot be blank");
            }
            return Settings.Default;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            this.M1panel.Enabled = Settings.Default.EnableM1;
            this.M2panel.Enabled = Settings.Default.EnableM2;
            this.M3panel.Enabled = Settings.Default.EnableM3;
            this.LockPanel.Enabled = Settings.Default.EnableLock;
            this.KickPanel.Enabled = Settings.Default.EnableKick;
            this.RestartPanel.Enabled = Settings.Default.EnableRestart;
            this.runPanel.Enabled = Settings.Default.EnableRun;
            string version = System.Windows.Forms.Application.ProductVersion;
            this.Text = String.Format("DZRCon DayZ Server Restarter {0}", version);
            Globals.CountdownLabel = countdownText;
            //System.Windows.Forms.MessageBox.Show("this.rconIP.Text:" + this.rconIP.Text);
        }

        private void rconPort_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click_1(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://dayzrussia.com/f/index.php?threads/4583/");
        }

        private void saveSettings(object sender, FormClosedEventArgs e)
        {
            saveSettings();
        }

        private void ResetToDefault(object sender, EventArgs e)
        {
            //Properties.Settings.Default.Reset();
            Properties.Settings.Default.Reset();
            Properties.Settings.Default.Save();
            //Application.Exit();
            System.Windows.Forms.MessageBox.Show("Restart to load defaults");
        }

        private void SaveIP(object sender, EventArgs e)
        {
            saveSettings();
        }

        private void savePort(object sender, EventArgs e)
        {
            saveSettings();
        }

        private void SavePass(object sender, EventArgs e)
        {
            saveSettings();
        }

        private void M1timeSave(object sender, EventArgs e)
        {
            saveSettings();
        }

        private void M2TimeSave(object sender, EventArgs e)
        {
            saveSettings();
        }

        private void M3TimeSave(object sender, EventArgs e)
        {
            saveSettings();
        }

        private void LockSave(object sender, EventArgs e)
        {
            saveSettings();
        }

        private void KickSave(object sender, EventArgs e)
        {
            saveSettings();
        }

        private void RestartSave(object sender, EventArgs e)
        {
            saveSettings();
        }

        private void RunBatSave(object sender, EventArgs e)
        {
            saveSettings();
        }

        private void repeatRestarts_CheckedChanged(object sender, EventArgs e)
        {
            //Settings.Default["Repeat"] = repeatRestarts.Checked;
            //System.Windows.Forms.MessageBox.Show(" repeatRestarts.Checked:" + repeatRestarts.Checked.ToString());
            saveSettings();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Globals.networkClient.Disconnect();
        }

        private void EnableRun_CheckedChanged(object sender, EventArgs e)
        {
            runPanel.Enabled = EnableRun.Checked;
            saveSettings();
        }

        private void EnableRestart_CheckedChanged(object sender, EventArgs e)
        {
            RestartPanel.Enabled = EnableRestart.Checked;
            saveSettings();
        }

        private void EnableKick_CheckedChanged(object sender, EventArgs e)
        {
            KickPanel.Enabled = EnableKick.Checked;
            saveSettings();
        }

        private void EnableLock_CheckedChanged(object sender, EventArgs e)
        {
            LockPanel.Enabled = EnableLock.Checked;
            saveSettings();
        }

        private void EnableM3_CheckedChanged(object sender, EventArgs e)
        {
            M3panel.Enabled = EnableM3.Checked;
            saveSettings();
        }

        private void EnableM2_CheckedChanged(object sender, EventArgs e)
        {
            M2panel.Enabled = EnableM2.Checked;
            saveSettings();
        }

        private void EnableM1_CheckedChanged(object sender, EventArgs e)
        {
            M1panel.Enabled = EnableM1.Checked;
            saveSettings();
        }

        private async void button4_Click(object sender, EventArgs e)
        {
            //Globals.RunningBat = true;
            ManualRun.Enabled = false;
            //Globals.RunBat = true;
            runBatFile(1000, true);
            await Task.Delay(1000);
            ManualRun.Enabled = true;

        }

        private void label11_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/dayz-russia/dzrcon");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Globals.CancelToken.Cancel();
        }

        private void countdownText_Click(object sender, EventArgs e)
        {

        }
    }
}
