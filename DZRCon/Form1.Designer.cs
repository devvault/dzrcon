﻿namespace DZRCon
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.StartProgram = new System.Windows.Forms.Button();
            this.RconConsole = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.PlayersCountPanel = new System.Windows.Forms.Panel();
            this.PlayerCount = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.RestartPanel = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.M1TextPanel = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.SchedulePanel = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.runPanel = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.KickPanel = new System.Windows.Forms.Panel();
            this.kciklabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.LockPanel = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.M3panel = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.M2panel = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.M1panel = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.countdownText = new System.Windows.Forms.Label();
            this.M2TextPanel = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.ManualRun = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.SettingsPanel = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.Message1Text = new System.Windows.Forms.TextBox();
            this.Message2Text = new System.Windows.Forms.TextBox();
            this.BatName = new System.Windows.Forms.TextBox();
            this.Message3Text = new System.Windows.Forms.TextBox();
            this.EnableRun = new System.Windows.Forms.CheckBox();
            this.EnableRestart = new System.Windows.Forms.CheckBox();
            this.EnableKick = new System.Windows.Forms.CheckBox();
            this.EnableLock = new System.Windows.Forms.CheckBox();
            this.EnableM3 = new System.Windows.Forms.CheckBox();
            this.EnableM2 = new System.Windows.Forms.CheckBox();
            this.EnableM1 = new System.Windows.Forms.CheckBox();
            this.RunBatTime = new System.Windows.Forms.TextBox();
            this.repeatRestarts = new System.Windows.Forms.CheckBox();
            this.KickTime = new System.Windows.Forms.TextBox();
            this.LockTime = new System.Windows.Forms.TextBox();
            this.Message3time = new System.Windows.Forms.TextBox();
            this.Message2time = new System.Windows.Forms.TextBox();
            this.Message1time = new System.Windows.Forms.TextBox();
            this.RestartTime = new System.Windows.Forms.TextBox();
            this.rconPass = new System.Windows.Forms.TextBox();
            this.rconIP = new System.Windows.Forms.TextBox();
            this.rconPort = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.PlayersCountPanel.SuspendLayout();
            this.RestartPanel.SuspendLayout();
            this.M1TextPanel.SuspendLayout();
            this.SchedulePanel.SuspendLayout();
            this.runPanel.SuspendLayout();
            this.KickPanel.SuspendLayout();
            this.LockPanel.SuspendLayout();
            this.M3panel.SuspendLayout();
            this.M2panel.SuspendLayout();
            this.M1panel.SuspendLayout();
            this.M2TextPanel.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SettingsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // StartProgram
            // 
            this.StartProgram.Location = new System.Drawing.Point(3, 47);
            this.StartProgram.Name = "StartProgram";
            this.StartProgram.Size = new System.Drawing.Size(75, 23);
            this.StartProgram.TabIndex = 0;
            this.StartProgram.Text = "Connect";
            this.StartProgram.UseVisualStyleBackColor = true;
            this.StartProgram.Click += new System.EventHandler(this.button1_Click);
            // 
            // RconConsole
            // 
            this.RconConsole.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RconConsole.Location = new System.Drawing.Point(3, 76);
            this.RconConsole.Name = "RconConsole";
            this.RconConsole.Size = new System.Drawing.Size(316, 295);
            this.RconConsole.TabIndex = 1;
            this.RconConsole.Text = "";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(244, 47);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(84, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Rcon port";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(138, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "RCon password";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.PlayersCountPanel);
            this.panel1.Controls.Add(this.rconPass);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.RconConsole);
            this.panel1.Controls.Add(this.StartProgram);
            this.panel1.Controls.Add(this.rconIP);
            this.panel1.Controls.Add(this.rconPort);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(324, 376);
            this.panel1.TabIndex = 9;
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(109, 344);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(197, 23);
            this.button5.TabIndex = 21;
            this.button5.Text = "tokenSource.Cancel();";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // PlayersCountPanel
            // 
            this.PlayersCountPanel.Controls.Add(this.PlayerCount);
            this.PlayersCountPanel.Controls.Add(this.label17);
            this.PlayersCountPanel.Location = new System.Drawing.Point(87, 49);
            this.PlayersCountPanel.Name = "PlayersCountPanel";
            this.PlayersCountPanel.Size = new System.Drawing.Size(152, 26);
            this.PlayersCountPanel.TabIndex = 17;
            this.PlayersCountPanel.Visible = false;
            // 
            // PlayerCount
            // 
            this.PlayerCount.AutoSize = true;
            this.PlayerCount.Dock = System.Windows.Forms.DockStyle.Left;
            this.PlayerCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlayerCount.Location = new System.Drawing.Point(128, 0);
            this.PlayerCount.Name = "PlayerCount";
            this.PlayerCount.Size = new System.Drawing.Size(17, 18);
            this.PlayerCount.TabIndex = 12;
            this.PlayerCount.Text = "0";
            this.PlayerCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Left;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(0, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(128, 18);
            this.label17.TabIndex = 13;
            this.label17.Text = "Current players:";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(286, -1);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(33, 23);
            this.button3.TabIndex = 18;
            this.button3.Text = "DC";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // RestartPanel
            // 
            this.RestartPanel.Controls.Add(this.RestartTime);
            this.RestartPanel.Controls.Add(this.label4);
            this.RestartPanel.Location = new System.Drawing.Point(9, 249);
            this.RestartPanel.Name = "RestartPanel";
            this.RestartPanel.Size = new System.Drawing.Size(175, 32);
            this.RestartPanel.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Restart Server";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label4.Click += new System.EventHandler(this.label4_Click_1);
            // 
            // M1TextPanel
            // 
            this.M1TextPanel.Controls.Add(this.Message1Text);
            this.M1TextPanel.Controls.Add(this.label5);
            this.M1TextPanel.Location = new System.Drawing.Point(-2, 0);
            this.M1TextPanel.Name = "M1TextPanel";
            this.M1TextPanel.Size = new System.Drawing.Size(263, 93);
            this.M1TextPanel.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Message #1";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // SchedulePanel
            // 
            this.SchedulePanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.SchedulePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SchedulePanel.Controls.Add(this.EnableRun);
            this.SchedulePanel.Controls.Add(this.button2);
            this.SchedulePanel.Controls.Add(this.EnableRestart);
            this.SchedulePanel.Controls.Add(this.EnableKick);
            this.SchedulePanel.Controls.Add(this.EnableLock);
            this.SchedulePanel.Controls.Add(this.EnableM3);
            this.SchedulePanel.Controls.Add(this.EnableM2);
            this.SchedulePanel.Controls.Add(this.EnableM1);
            this.SchedulePanel.Controls.Add(this.runPanel);
            this.SchedulePanel.Controls.Add(this.repeatRestarts);
            this.SchedulePanel.Controls.Add(this.label12);
            this.SchedulePanel.Controls.Add(this.KickPanel);
            this.SchedulePanel.Controls.Add(this.label10);
            this.SchedulePanel.Controls.Add(this.LockPanel);
            this.SchedulePanel.Controls.Add(this.M3panel);
            this.SchedulePanel.Controls.Add(this.M2panel);
            this.SchedulePanel.Controls.Add(this.M1panel);
            this.SchedulePanel.Controls.Add(this.RestartPanel);
            this.SchedulePanel.Location = new System.Drawing.Point(329, 4);
            this.SchedulePanel.Name = "SchedulePanel";
            this.SchedulePanel.Size = new System.Drawing.Size(211, 376);
            this.SchedulePanel.TabIndex = 12;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(9, 325);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(102, 39);
            this.button2.TabIndex = 14;
            this.button2.Text = "Restore Default\r\nSettings and Exit";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.ResetToDefault);
            // 
            // runPanel
            // 
            this.runPanel.Controls.Add(this.RunBatTime);
            this.runPanel.Controls.Add(this.label13);
            this.runPanel.Enabled = false;
            this.runPanel.Location = new System.Drawing.Point(9, 287);
            this.runPanel.Name = "runPanel";
            this.runPanel.Size = new System.Drawing.Size(175, 32);
            this.runPanel.TabIndex = 11;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 10);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Run bat on disc.";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(109, 233);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "In X minutes";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // KickPanel
            // 
            this.KickPanel.Controls.Add(this.kciklabel);
            this.KickPanel.Controls.Add(this.KickTime);
            this.KickPanel.Location = new System.Drawing.Point(9, 181);
            this.KickPanel.Name = "KickPanel";
            this.KickPanel.Size = new System.Drawing.Size(175, 32);
            this.KickPanel.TabIndex = 13;
            // 
            // kciklabel
            // 
            this.kciklabel.AutoSize = true;
            this.kciklabel.Location = new System.Drawing.Point(4, 9);
            this.kciklabel.Name = "kciklabel";
            this.kciklabel.Size = new System.Drawing.Size(79, 13);
            this.kciklabel.TabIndex = 2;
            this.kciklabel.Text = "Kick All Players";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(100, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 26);
            this.label10.TabIndex = 14;
            this.label10.Text = "Minutes before\r\nthe next event";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LockPanel
            // 
            this.LockPanel.Controls.Add(this.label9);
            this.LockPanel.Controls.Add(this.LockTime);
            this.LockPanel.Location = new System.Drawing.Point(9, 143);
            this.LockPanel.Name = "LockPanel";
            this.LockPanel.Size = new System.Drawing.Size(175, 32);
            this.LockPanel.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Lock Server";
            // 
            // M3panel
            // 
            this.M3panel.Controls.Add(this.Message3time);
            this.M3panel.Controls.Add(this.label8);
            this.M3panel.Location = new System.Drawing.Point(9, 105);
            this.M3panel.Name = "M3panel";
            this.M3panel.Size = new System.Drawing.Size(174, 32);
            this.M3panel.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Message #3";
            // 
            // M2panel
            // 
            this.M2panel.Controls.Add(this.label6);
            this.M2panel.Controls.Add(this.Message2time);
            this.M2panel.Location = new System.Drawing.Point(9, 67);
            this.M2panel.Name = "M2panel";
            this.M2panel.Size = new System.Drawing.Size(175, 32);
            this.M2panel.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Message #2";
            // 
            // M1panel
            // 
            this.M1panel.Controls.Add(this.label7);
            this.M1panel.Controls.Add(this.Message1time);
            this.M1panel.Location = new System.Drawing.Point(9, 29);
            this.M1panel.Name = "M1panel";
            this.M1panel.Size = new System.Drawing.Size(175, 32);
            this.M1panel.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Message #1";
            // 
            // countdownText
            // 
            this.countdownText.AutoSize = true;
            this.countdownText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.countdownText.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.countdownText.Location = new System.Drawing.Point(341, 9);
            this.countdownText.Name = "countdownText";
            this.countdownText.Size = new System.Drawing.Size(76, 20);
            this.countdownText.TabIndex = 21;
            this.countdownText.Text = "-00:00:00";
            this.countdownText.Click += new System.EventHandler(this.countdownText_Click);
            // 
            // M2TextPanel
            // 
            this.M2TextPanel.Controls.Add(this.Message2Text);
            this.M2TextPanel.Controls.Add(this.label14);
            this.M2TextPanel.Location = new System.Drawing.Point(-2, 92);
            this.M2TextPanel.Name = "M2TextPanel";
            this.M2TextPanel.Size = new System.Drawing.Size(263, 93);
            this.M2TextPanel.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Message #2";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.Message3Text);
            this.panel11.Controls.Add(this.label15);
            this.panel11.Location = new System.Drawing.Point(-2, 185);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(263, 93);
            this.panel11.TabIndex = 12;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Message #3";
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.ManualRun);
            this.panel12.Controls.Add(this.BatName);
            this.panel12.Location = new System.Drawing.Point(-2, 267);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(263, 29);
            this.panel12.TabIndex = 12;
            // 
            // ManualRun
            // 
            this.ManualRun.Location = new System.Drawing.Point(6, 3);
            this.ManualRun.Name = "ManualRun";
            this.ManualRun.Size = new System.Drawing.Size(79, 23);
            this.ManualRun.TabIndex = 16;
            this.ManualRun.Text = "Run bat file";
            this.ManualRun.UseVisualStyleBackColor = true;
            this.ManualRun.Click += new System.EventHandler(this.button4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::DZRCon.Properties.Resources.DZR_logo_avatar1;
            this.pictureBox1.Location = new System.Drawing.Point(678, 303);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 86);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // SettingsPanel
            // 
            this.SettingsPanel.Controls.Add(this.M1TextPanel);
            this.SettingsPanel.Controls.Add(this.M2TextPanel);
            this.SettingsPanel.Controls.Add(this.panel12);
            this.SettingsPanel.Controls.Add(this.panel11);
            this.SettingsPanel.Location = new System.Drawing.Point(542, 4);
            this.SettingsPanel.Name = "SettingsPanel";
            this.SettingsPanel.Size = new System.Drawing.Size(263, 320);
            this.SettingsPanel.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label11.ForeColor = System.Drawing.Color.MediumBlue;
            this.label11.Location = new System.Drawing.Point(546, 366);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(107, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Source on gitlab.com";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // Message1Text
            // 
            this.Message1Text.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "Message1Text", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Message1Text.Location = new System.Drawing.Point(6, 16);
            this.Message1Text.Multiline = true;
            this.Message1Text.Name = "Message1Text";
            this.Message1Text.Size = new System.Drawing.Size(248, 64);
            this.Message1Text.TabIndex = 1;
            this.Message1Text.Text = global::DZRCon.Properties.Settings.Default.Message1Text;
            // 
            // Message2Text
            // 
            this.Message2Text.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "Message2Text", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Message2Text.Location = new System.Drawing.Point(6, 16);
            this.Message2Text.Multiline = true;
            this.Message2Text.Name = "Message2Text";
            this.Message2Text.Size = new System.Drawing.Size(248, 64);
            this.Message2Text.TabIndex = 1;
            this.Message2Text.Text = global::DZRCon.Properties.Settings.Default.Message2Text;
            // 
            // BatName
            // 
            this.BatName.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "BatName", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.BatName.Location = new System.Drawing.Point(91, 4);
            this.BatName.Name = "BatName";
            this.BatName.Size = new System.Drawing.Size(163, 20);
            this.BatName.TabIndex = 1;
            this.BatName.Text = global::DZRCon.Properties.Settings.Default.BatName;
            // 
            // Message3Text
            // 
            this.Message3Text.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "Message3Text", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Message3Text.Location = new System.Drawing.Point(6, 16);
            this.Message3Text.Multiline = true;
            this.Message3Text.Name = "Message3Text";
            this.Message3Text.Size = new System.Drawing.Size(248, 64);
            this.Message3Text.TabIndex = 1;
            this.Message3Text.Text = global::DZRCon.Properties.Settings.Default.Message3Text;
            // 
            // EnableRun
            // 
            this.EnableRun.AutoSize = true;
            this.EnableRun.Checked = global::DZRCon.Properties.Settings.Default.EnableRun;
            this.EnableRun.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DZRCon.Properties.Settings.Default, "EnableRun", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.EnableRun.Location = new System.Drawing.Point(190, 298);
            this.EnableRun.Name = "EnableRun";
            this.EnableRun.Size = new System.Drawing.Size(15, 14);
            this.EnableRun.TabIndex = 20;
            this.EnableRun.UseVisualStyleBackColor = true;
            this.EnableRun.CheckedChanged += new System.EventHandler(this.EnableRun_CheckedChanged);
            // 
            // EnableRestart
            // 
            this.EnableRestart.AutoSize = true;
            this.EnableRestart.Checked = global::DZRCon.Properties.Settings.Default.EnableRestart;
            this.EnableRestart.CheckState = System.Windows.Forms.CheckState.Checked;
            this.EnableRestart.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DZRCon.Properties.Settings.Default, "EnableRestart", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.EnableRestart.Location = new System.Drawing.Point(190, 260);
            this.EnableRestart.Name = "EnableRestart";
            this.EnableRestart.Size = new System.Drawing.Size(15, 14);
            this.EnableRestart.TabIndex = 20;
            this.EnableRestart.UseVisualStyleBackColor = true;
            this.EnableRestart.CheckedChanged += new System.EventHandler(this.EnableRestart_CheckedChanged);
            // 
            // EnableKick
            // 
            this.EnableKick.AutoSize = true;
            this.EnableKick.Checked = global::DZRCon.Properties.Settings.Default.EnableKick;
            this.EnableKick.CheckState = System.Windows.Forms.CheckState.Checked;
            this.EnableKick.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DZRCon.Properties.Settings.Default, "EnableKick", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.EnableKick.Location = new System.Drawing.Point(190, 191);
            this.EnableKick.Name = "EnableKick";
            this.EnableKick.Size = new System.Drawing.Size(15, 14);
            this.EnableKick.TabIndex = 20;
            this.EnableKick.UseVisualStyleBackColor = true;
            this.EnableKick.CheckedChanged += new System.EventHandler(this.EnableKick_CheckedChanged);
            // 
            // EnableLock
            // 
            this.EnableLock.AutoSize = true;
            this.EnableLock.Checked = global::DZRCon.Properties.Settings.Default.EnableLock;
            this.EnableLock.CheckState = System.Windows.Forms.CheckState.Checked;
            this.EnableLock.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DZRCon.Properties.Settings.Default, "EnableLock", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.EnableLock.Location = new System.Drawing.Point(190, 153);
            this.EnableLock.Name = "EnableLock";
            this.EnableLock.Size = new System.Drawing.Size(15, 14);
            this.EnableLock.TabIndex = 20;
            this.EnableLock.UseVisualStyleBackColor = true;
            this.EnableLock.CheckedChanged += new System.EventHandler(this.EnableLock_CheckedChanged);
            // 
            // EnableM3
            // 
            this.EnableM3.AutoSize = true;
            this.EnableM3.Checked = global::DZRCon.Properties.Settings.Default.EnableM3;
            this.EnableM3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.EnableM3.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DZRCon.Properties.Settings.Default, "EnableM3", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.EnableM3.Location = new System.Drawing.Point(190, 115);
            this.EnableM3.Name = "EnableM3";
            this.EnableM3.Size = new System.Drawing.Size(15, 14);
            this.EnableM3.TabIndex = 20;
            this.EnableM3.UseVisualStyleBackColor = true;
            this.EnableM3.CheckedChanged += new System.EventHandler(this.EnableM3_CheckedChanged);
            // 
            // EnableM2
            // 
            this.EnableM2.AutoSize = true;
            this.EnableM2.Checked = global::DZRCon.Properties.Settings.Default.EnableM2;
            this.EnableM2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.EnableM2.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DZRCon.Properties.Settings.Default, "EnableM2", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.EnableM2.Location = new System.Drawing.Point(190, 77);
            this.EnableM2.Name = "EnableM2";
            this.EnableM2.Size = new System.Drawing.Size(15, 14);
            this.EnableM2.TabIndex = 20;
            this.EnableM2.UseVisualStyleBackColor = true;
            this.EnableM2.CheckedChanged += new System.EventHandler(this.EnableM2_CheckedChanged);
            // 
            // EnableM1
            // 
            this.EnableM1.AutoSize = true;
            this.EnableM1.Checked = global::DZRCon.Properties.Settings.Default.EnableM1;
            this.EnableM1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.EnableM1.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DZRCon.Properties.Settings.Default, "EnableM1", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.EnableM1.Location = new System.Drawing.Point(190, 39);
            this.EnableM1.Name = "EnableM1";
            this.EnableM1.Size = new System.Drawing.Size(15, 14);
            this.EnableM1.TabIndex = 19;
            this.EnableM1.UseVisualStyleBackColor = true;
            this.EnableM1.CheckedChanged += new System.EventHandler(this.EnableM1_CheckedChanged);
            // 
            // RunBatTime
            // 
            this.RunBatTime.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "RunBatTime", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.RunBatTime.Location = new System.Drawing.Point(94, 6);
            this.RunBatTime.Name = "RunBatTime";
            this.RunBatTime.Size = new System.Drawing.Size(74, 20);
            this.RunBatTime.TabIndex = 1;
            this.RunBatTime.Text = global::DZRCon.Properties.Settings.Default.RunBatTime;
            this.RunBatTime.ModifiedChanged += new System.EventHandler(this.RunBatSave);
            // 
            // repeatRestarts
            // 
            this.repeatRestarts.AutoSize = true;
            this.repeatRestarts.Checked = global::DZRCon.Properties.Settings.Default.Repeat;
            this.repeatRestarts.CheckState = System.Windows.Forms.CheckState.Checked;
            this.repeatRestarts.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::DZRCon.Properties.Settings.Default, "Repeat", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.repeatRestarts.Location = new System.Drawing.Point(122, 325);
            this.repeatRestarts.Name = "repeatRestarts";
            this.repeatRestarts.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.repeatRestarts.Size = new System.Drawing.Size(61, 17);
            this.repeatRestarts.TabIndex = 16;
            this.repeatRestarts.Text = "Repeat";
            this.repeatRestarts.UseVisualStyleBackColor = true;
            this.repeatRestarts.Visible = false;
            this.repeatRestarts.CheckedChanged += new System.EventHandler(this.repeatRestarts_CheckedChanged);
            // 
            // KickTime
            // 
            this.KickTime.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "KickTime", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.KickTime.Location = new System.Drawing.Point(94, 6);
            this.KickTime.Name = "KickTime";
            this.KickTime.Size = new System.Drawing.Size(75, 20);
            this.KickTime.TabIndex = 1;
            this.KickTime.Text = global::DZRCon.Properties.Settings.Default.KickTime;
            this.KickTime.ModifiedChanged += new System.EventHandler(this.KickSave);
            // 
            // LockTime
            // 
            this.LockTime.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "LockTime", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.LockTime.Location = new System.Drawing.Point(94, 6);
            this.LockTime.Name = "LockTime";
            this.LockTime.Size = new System.Drawing.Size(75, 20);
            this.LockTime.TabIndex = 1;
            this.LockTime.Text = global::DZRCon.Properties.Settings.Default.LockTime;
            this.LockTime.ModifiedChanged += new System.EventHandler(this.LockSave);
            // 
            // Message3time
            // 
            this.Message3time.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "Message3Time", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Message3time.Location = new System.Drawing.Point(94, 6);
            this.Message3time.Name = "Message3time";
            this.Message3time.Size = new System.Drawing.Size(74, 20);
            this.Message3time.TabIndex = 1;
            this.Message3time.Text = global::DZRCon.Properties.Settings.Default.Message3Time;
            this.Message3time.ModifiedChanged += new System.EventHandler(this.M3TimeSave);
            // 
            // Message2time
            // 
            this.Message2time.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "Message2Time", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Message2time.Location = new System.Drawing.Point(94, 6);
            this.Message2time.Name = "Message2time";
            this.Message2time.Size = new System.Drawing.Size(74, 20);
            this.Message2time.TabIndex = 1;
            this.Message2time.Text = global::DZRCon.Properties.Settings.Default.Message2Time;
            this.Message2time.ModifiedChanged += new System.EventHandler(this.M2TimeSave);
            // 
            // Message1time
            // 
            this.Message1time.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "Message1Time", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.Message1time.Location = new System.Drawing.Point(94, 6);
            this.Message1time.Name = "Message1time";
            this.Message1time.Size = new System.Drawing.Size(74, 20);
            this.Message1time.TabIndex = 1;
            this.Message1time.Text = global::DZRCon.Properties.Settings.Default.Message1Time;
            this.Message1time.ModifiedChanged += new System.EventHandler(this.M1timeSave);
            // 
            // RestartTime
            // 
            this.RestartTime.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "RestartTime", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.RestartTime.Location = new System.Drawing.Point(94, 6);
            this.RestartTime.Name = "RestartTime";
            this.RestartTime.Size = new System.Drawing.Size(74, 20);
            this.RestartTime.TabIndex = 1;
            this.RestartTime.Text = global::DZRCon.Properties.Settings.Default.RestartTime;
            this.RestartTime.ModifiedChanged += new System.EventHandler(this.RestartSave);
            // 
            // rconPass
            // 
            this.rconPass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rconPass.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "rconPass", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.rconPass.Location = new System.Drawing.Point(141, 21);
            this.rconPass.Name = "rconPass";
            this.rconPass.PasswordChar = '*';
            this.rconPass.Size = new System.Drawing.Size(178, 20);
            this.rconPass.TabIndex = 7;
            this.rconPass.Text = global::DZRCon.Properties.Settings.Default.rconPass;
            this.rconPass.ModifiedChanged += new System.EventHandler(this.SavePass);
            // 
            // rconIP
            // 
            this.rconIP.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "rconIP", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.rconIP.Location = new System.Drawing.Point(3, 21);
            this.rconIP.Name = "rconIP";
            this.rconIP.Size = new System.Drawing.Size(80, 20);
            this.rconIP.TabIndex = 3;
            this.rconIP.Text = global::DZRCon.Properties.Settings.Default.rconIP;
            this.rconIP.ModifiedChanged += new System.EventHandler(this.SaveIP);
            // 
            // rconPort
            // 
            this.rconPort.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::DZRCon.Properties.Settings.Default, "rconPort", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.rconPort.Location = new System.Drawing.Point(88, 21);
            this.rconPort.Name = "rconPort";
            this.rconPort.Size = new System.Drawing.Size(48, 20);
            this.rconPort.TabIndex = 4;
            this.rconPort.Text = global::DZRCon.Properties.Settings.Default.rconPort;
            this.rconPort.ModifiedChanged += new System.EventHandler(this.savePort);
            this.rconPort.TextChanged += new System.EventHandler(this.rconPort_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(800, 384);
            this.Controls.Add(this.countdownText);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.SettingsPanel);
            this.Controls.Add(this.SchedulePanel);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "DZRCon DayZ Server Restarter";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.saveSettings);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.PlayersCountPanel.ResumeLayout(false);
            this.PlayersCountPanel.PerformLayout();
            this.RestartPanel.ResumeLayout(false);
            this.RestartPanel.PerformLayout();
            this.M1TextPanel.ResumeLayout(false);
            this.M1TextPanel.PerformLayout();
            this.SchedulePanel.ResumeLayout(false);
            this.SchedulePanel.PerformLayout();
            this.runPanel.ResumeLayout(false);
            this.runPanel.PerformLayout();
            this.KickPanel.ResumeLayout(false);
            this.KickPanel.PerformLayout();
            this.LockPanel.ResumeLayout(false);
            this.LockPanel.PerformLayout();
            this.M3panel.ResumeLayout(false);
            this.M3panel.PerformLayout();
            this.M2panel.ResumeLayout(false);
            this.M2panel.PerformLayout();
            this.M1panel.ResumeLayout(false);
            this.M1panel.PerformLayout();
            this.M2TextPanel.ResumeLayout(false);
            this.M2TextPanel.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.SettingsPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button StartProgram;
        private System.Windows.Forms.RichTextBox RconConsole;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox rconIP;
        private System.Windows.Forms.TextBox rconPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox rconPass;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel RestartPanel;
        private System.Windows.Forms.TextBox RestartTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel M1TextPanel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel SchedulePanel;
        private System.Windows.Forms.TextBox Message1Text;
        private System.Windows.Forms.Panel M1panel;
        private System.Windows.Forms.TextBox Message1time;
        private System.Windows.Forms.Panel M3panel;
        private System.Windows.Forms.TextBox Message3time;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel M2panel;
        private System.Windows.Forms.TextBox Message2time;
        private System.Windows.Forms.Panel runPanel;
        private System.Windows.Forms.TextBox RunBatTime;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox repeatRestarts;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel KickPanel;
        private System.Windows.Forms.Label kciklabel;
        private System.Windows.Forms.TextBox KickTime;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel LockPanel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox LockTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel M2TextPanel;
        private System.Windows.Forms.TextBox Message2Text;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.TextBox Message3Text;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label PlayerCount;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox BatName;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel PlayersCountPanel;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox EnableRun;
        private System.Windows.Forms.CheckBox EnableRestart;
        private System.Windows.Forms.CheckBox EnableKick;
        private System.Windows.Forms.CheckBox EnableLock;
        private System.Windows.Forms.CheckBox EnableM3;
        private System.Windows.Forms.CheckBox EnableM2;
        private System.Windows.Forms.CheckBox EnableM1;
        private System.Windows.Forms.Panel SettingsPanel;
        private System.Windows.Forms.Button ManualRun;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label countdownText;
        private System.Windows.Forms.Button button5;
    }
}

