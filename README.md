# DZRCon DayZ Server Restarter
This app waits the defined time and launches a series of commands to the server: message, lock, kick, shutdown.

## If you liked the app
[![](donate.png)](https://dayzrussia.com/f/index.php?threads/2985/)
Developed by Mikhail from [DayZRussia.com](http://DayZRussia.com)

## Description
This was specifically designed for DZR servers where whe need the following restart procedure:

1. Wait 4 hours.
1. Warn players that resart is in 15 minutes.
1. Warn players 5 minutes before restart.
1. Warn players 1 minute before restart.
1. Lock server.
1. Kick all players.
1. Wait 5 minutes for server to synchronize inventories and prevent duping.
1. Shutdown the server.
1. Run custom launch bat file (if needed).


## How to use

This app uses a rundown logics for the restart cycle. It is fully linear. The rundown order cannot be changed. Only certain items can be disabled.
1. Build source or [download binaries](https://gitlab.com/soundgot/dzrcon/-/blob/main/DZRCon/bin/Release/DZRcon.zip).
1. Launch app.
1. Enter your RCon info. 
 - You need 127.0.0.1 as the IP if you launch this app on the same machine as the server runs.
 - Or use the external internet IP if the app should restart remotely. Mind the port! It is not the gameport, it's RCon port that is shown in the console when you launch your server.
1. Set the first delay for Message #1. It will be your main delay. Set it to 240 minutes and it will wait for 4 hours. Then it fires Message one. This message may warn players that restart is oncoming.
1. Set other delays or disable if not needed.
1. Define the shutdown delay.
1. Define a bat file that will run after restart.
   - Useful if you don't have Omega or any other app to automatically restart your server.
   - Example bat file here https://gitlab.com/dayz-russia/dzrcon/-/tree/main/Bat%20file. 
   - Launch the app as admin or killing process from bat will be impossible.
1. Hit Connect.
1. Rest while the app is working :)

## Known bugs

- You cannot restart sequence. Only exit the app. Once started, the cycle will loop forever.
- If your server was down for some time, the app will spam some 5-10 "log in" messages to your server console when it starts again.
- When server is shut down and app is disconnected, some rundown commands may still follow the timer and get green, but will not reach server. Just delayed execution bug.
- Sometimes empty or incorrect values in the fields may crash the app.


## Info and permissions

> This app was made for the DayZ Community as open-source to contribute to help the DayZ Community, make it grow and learn modding and development. So this app or the derived works cannot be sold in any way and should always remain open-source and accessible to the Community.
> You may unpack, repack, change, derive anything with attribution as a link to this page with.

- [Donate](https://dayzrussia.com/f/index.php?threads/2985/) if you like the app. Thanks!
- [Forum](https://dayzrussia.com/f/index.php?threads/4583/)
- [Discord](https://discord.com/invite/4qSh5SQ4eK)
- Thanks BytexDigital for the [awesome RCon protocol](https://github.com/BytexDigital/BytexDigital.BattlEye.Rcon)

P.s.
I'm not very experienced in C# and using threads. You see, I could not even implement cycle restart (it would multiply commands for some reason). You will have to exit the app to start over. The app can be improved in many ways by an experienced programmer.
