@echo off 
echo "Use this bat procedure in case you don't have Omega or ther software that will strat your server automatically after being shutdown."
echo "Requires the app to be launched as administrator.
echo "Kill any accidentally running servers before launching a new one."
taskkill /f /im DayZServer_x64.exe

echo "Start a new server."
start "" "C:\SteamLibrary\steamapps\common\DayZServer\DayZServer_x64.exe" -config=serverDZ.cfg -port=2302 "-profiles=C:\SteamLibrary\steamapps\common\DayZServer\profiles" -dologs -adminlog -netlog -freezecheck "-BEpath=C:\SteamLibrary\steamapps\common\DayZServer\battleye"

echo "Now wait the app to connect. This window can be closed."
pause